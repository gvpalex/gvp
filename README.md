Alexander completed his studies at the Faculty of Law of the University of Lisbon and is currently enrolled in the Portuguese Bar Association.

Initially, he gained professional experience at the law firm Caiado Guerreiro (2013), where he was responsible for the German Desk specialized in assisting german clients. Since June 2016 he has worked at the law firm Nogueira Pegas Advogados, responsible for tax law and immigration law.

Over the past years, he has accompanied several clients in the process of obtaining the NHR (non-habitual resident), Portuguese citizenship, and has been continually monitoring applications for residence permits for investment purposes (Golden Visa).

Website : https://golden-visa-portugal.info/